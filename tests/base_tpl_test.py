"""Test chart templates."""

import os
import shutil

import yaml
from rpycihelpers.execute import execute_command
from rpycihelpers.files import create_dir, create_file
from rpycihelpers.foldered_test import FolderedPyTest


class BaseTplTest(FolderedPyTest):
    """Test Chart Templates."""

    def chart_with_tpl(self, chartname: str = "rdeploy") -> None:
        """Create test chart with the tpl directory.

        :param chartname: The dirname of the chart

        """
        current_dir = os.path.dirname(__file__)
        execute_command("helm create test_chart")
        shutil.rmtree("test_chart/templates")
        create_dir("test_chart/features")
        shutil.copytree(
            current_dir + f"/../charts/{chartname}/templates/tpl",  # noqa: WPS336
            "test_chart/templates/tpl",
        )

    def chart_template(self, file_content: str) -> None:
        """Create the chart template file.

        :param file_content: The file content of the template
        """
        create_file("test_chart/templates/template.yaml", file_content)

    def run_helm_template(self) -> str:
        """Run helm template generation.

        :returns: the helm chart string result
        """
        command = "helm template test-release -n test-ns test_chart"
        exec_result = execute_command(command)

        assert exec_result[0] == 0, "{}\n DEBUG-FLAG:\n {}".format(
            exec_result[2], execute_command(f"{command} --debug")[2]
        )

        return exec_result[1]

    def run_helm_template_dict(self) -> dict:
        """Run helm template and read the yaml result into a dict.

        :returns: Dictionary from yaml
        """
        stdout = self.run_helm_template()
        try:
            return yaml.safe_load(stdout)
        except Exception as e:
            print(e)

    def create_feature(self, feature_name: str, file_content: str) -> None:
        """Create a feature with the name and a test.yaml with the file_content.

        :param feature_name: The feature name
        :param file_content: The content of test.yaml
        """
        directory = f"test_chart/features/{feature_name}"
        create_dir(directory)
        create_file(f"{directory}/test.yaml", file_content)

    def set_values(self, value_string: str) -> None:
        """Set values in the test_chart.

        :param value_string: The content of the values.yaml
        """
        with open("test_chart/values.yaml", "w") as values_file:
            values_file.write(value_string)

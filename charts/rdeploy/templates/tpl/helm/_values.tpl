{{/*
 Processing values
*/}}


{{- define "rdeploy.helm.values" }}
{{- $values_file := (printf "features/%s/%s" .rd_feature.name "values.yaml") }}
{{- $values_content_string := .Files.Get $values_file -}}


{{ $spec := (dict) }}

{{- if (or (hasKey .rd_feature "values")  $values_content_string) }}
{{- $values_content_string :=
	include "rdeploy.templates.fill"
	(dict
		"text"
			($values_content_string)
		"context" .) }}
{{- $values_content_map :=  $values_content_string | fromYaml -}}
{{- $values_content_map := (mergeOverwrite $values_content_map (default (dict) .rd_feature.values)) -}}

{{- $_ := set $spec "values" $values_content_map }}
{{- end }}

{{- if (hasKey .rd_feature "valuesFrom") }}
{{- $_ := set $spec "valuesFrom" .rd_feature.valuesFrom }}
{{- end }}

{{- $spec | toYaml -}}
{{- end }}

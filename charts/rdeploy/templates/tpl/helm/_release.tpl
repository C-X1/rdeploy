{{/*
 Process helm release files
 */}}


{{- define "rdeploy.helm.release" }}

{{- $release_file := (printf "features/%s/%s" .rd_feature.name "release.yaml") }}
{{- $release_content_string := .Files.Get $release_file -}}

{{- /* Output */ -}}

{{- if $release_content_string }}

{{- $release_content_string := include "rdeploy.templates.fill" (dict "text" $release_content_string "context" .) }}
{{- $release_content_map :=  $release_content_string | fromYaml -}}
{{- $chart := $release_content_map.spec.chart }}

{{- $_ := set $release_content_map "metadata" (dict) }}
{{- $metadata := $release_content_map.metadata }}
{{- $_ := set $metadata "name"  .rd_feature._release_name }}
{{- $_ := set $metadata "namespace" .rd_feature._namespace -}}

{{- $_ := set $chart "spec" (mergeOverwrite $chart.spec (default (dict) .rd_feature.chart_spec)) -}}

{{- $_ := set $chart.spec "interval" (default $chart.spec.interval (default .Values.default.reconcile_interval .rd_feature.interval)) -}}

{{- $release_content_map := (mergeOverwrite $release_content_map (default (dict) .rd_feature.release_spec)) -}}

{{- $spec_values := $release_content_map.spec }}
{{- $spec_values := mergeOverwrite $spec_values (include "rdeploy.helm.values" .  | fromYaml) -}}
{{/*
---
## release.yaml of {{ .kind }}[{{ .index }}]
## Source: {{ $release_file }}
*/}}

{{ $release_content_map | toYaml -}}


{{- end -}}{{/*IF RELEASE_CONTENT_STRING*/}}

{{- end -}}{{/*DEFINE*/}}

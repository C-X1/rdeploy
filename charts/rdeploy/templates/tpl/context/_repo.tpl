{{- define "rdeploy.context.repo" }}

{{- $repo_file_content := .Files.Get (printf "features/%s/repos.yaml" .rd_feature.name) }}
{{- $repos := list }}

{{- if $repo_file_content }}

{{- $repo_docs := dict }}
{{- include "rdeploy.read_yaml_with_context"
    (dict
        "context" .
        "filepath" "repo"
        "bytes" $repo_file_content
        "output" $repo_docs)  -}}


{{- range $content := $repo_docs.repo }}
{{ $_ := set $content.metadata "namespace" $.rd_feature._repo_namespace }}
{{- $sha1_sum := $content | toString | sha1sum }}
{{- $name := print $.Release.Name "-" $content.metadata.name "-" ($sha1_sum | trunc 7 | trunc 63 ) }}
{{- $current_repo := dict }}

{{ $_ := set $current_repo "name" $name }}
{{ $_ := set $content.metadata "name" $name }}
{{ $_ := set $current_repo "manifest" $content }}

{{- $repos = append $repos $current_repo }}

{{- end }}
{{- end }}


{{ $_ := set .rd_feature "repos" $repos }}
{{- end }}

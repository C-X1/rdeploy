{{/*
 Create the context for processing a feature to fill out all files.
 */}}

{{- define "rdeploy.context.feature" }}

{{- $plain_context := merge dict . }}
{{- $_ := unset $plain_context "rd" }}
{{- $_ := unset $plain_context "rd_feature" }}

{{- $feature_config := include "rdeploy.templates.fill" (dict "text" (.rd_feature.config | toYaml) "context" .) | fromYaml }}

{{- $_ := set $feature_config "_release_name"
    (default $feature_config.name $feature_config.release_name) }}

{{- $_ := set $feature_config "_namespace"
    (default .Release.Namespace $feature_config.namespace) }}

{{ if .rd_feature.is_infra }}
{{- $_ := set $feature_config "_namespace"
    (default .Release.Namespace
        (default .Values.default.infra_namespace $feature_config.namespace)
    )
}}
{{- end }}


{{- $_ := set $feature_config "_repo_namespace"
    (default $feature_config._namespace
        (default .Values.default.repo_namespace
            $feature_config.repo_namespace))
}}

{{- $_ := set $feature_config "is_infra" .rd_feature.is_infra }}
{{- $_ := set $plain_context "rd_feature" $feature_config }}

{{  $_ := set $feature_config "_repo_namespace" (include "rdeploy.templates.fill" (dict "text" $feature_config._repo_namespace "context" $plain_context)) }}

{{- $defaults := (include "rdeploy.context.feature_defaults" $plain_context) | fromYaml }}
{{  $_ := set $feature_config "defaults" $defaults  }}



{{ $cvalues := .Files.Get (printf "features/%s/cvalues.yaml" $feature_config.name) }}
{{- $cvalues = include "rdeploy.templates.fill"
        (dict
            "text" $cvalues
            "context" $plain_context ) | fromYaml }}
{{- $_ := set $feature_config "cvalues" $cvalues }}

{{- include "rdeploy.context.repo" $plain_context -}}



{{ $sitemap := .Files.Get (printf "features/%s/sitemap.yaml" $feature_config.name) }}
{{- $sitemap = include "rdeploy.templates.fill"
        (dict
            "text" $sitemap
            "context" $plain_context ) | fromYamlArray }}
{{- $_ := set $feature_config "sitemap" $sitemap }}

{{- $templates := dict }}
{{- include "rdeploy.read_all_templates"
(dict "context" $plain_context
"output" $templates
)
}}

{{- $feature_values := merge dict .Values }}
{{- $_ := unset $feature_values "features" }}
{{- $_ := unset $feature_values "infrastructure" }}
{{- $feature_values := include "rdeploy.templates.fill"
    (dict
        "text" ($feature_values | toYaml)
        "context" $plain_context) | fromYaml
}}

{{- $_ := set $feature_config "templates" $templates }}
{{- $_ := set $feature_config "Values" $feature_values }}
{{- $_ := set $feature_config "release" ((include "rdeploy.helm.release" $plain_context) | fromYaml) }}

{{- if .rd_feature.is_infra }}
    {{- $_ := set $.rd "infra" (append $.rd.infra $feature_config) }}
{{- else -}}{{/*IF IS_INFRA*/}}
    {{- $_ := set $.rd "features" (append $.rd.features $feature_config) }}
{{- end }}{{/*IF IS_INFRA*/}}

{{- end }}{{/*DEFINE*/}}

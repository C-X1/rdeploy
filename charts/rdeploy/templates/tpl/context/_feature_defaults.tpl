{{/*
Context Feature Defaults
*/}}

{{- define "rdeploy.context.feature_defaults" }}

{{- $feature_name := .rd_feature.name }}
{{- $feature_defaults := .Values.feature_defaults }}


{{- if  $feature_defaults }}

{{- $feature_defaults = index $feature_defaults $feature_name }}
{{- (include "rdeploy.templates.fill"
	(dict "text" ($feature_defaults | toYaml)
		  "context" .)) -}}

{{- end }}{{/*IF FEATURE_DEFAULTS*/}}

{{- end }}{{/*DEFINE*/}}

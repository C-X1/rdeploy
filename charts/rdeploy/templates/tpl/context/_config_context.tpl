{{/*
 Context creation for final output
 */}}

{{- define "rdeploy.context.config_context" }}

{{- $context := merge dict . }}       {{/* Get default context */}}
{{- $rd := dict
"features" list
"infra" list
"repos" dict
}}
{{- $rd_feature := dict
"is_infra" true
}}
{{- $_ := set $context "rd" $rd }}                   {{/* Add dictionary for rdeploy output */}}
{{- $_ := set $context "rd_feature" $rd_feature }}   {{/* Add dictionary for rdeploy feature */}}

{{- range .Values.infrastructure.features }}
{{- $_ := set $context.rd_feature "config" . }}
{{- include "rdeploy.context.feature" $context }}
{{- end }}

{{- $_ := set $context.rd_feature "is_infra" false }}
{{- range .Values.features }}
{{- $_ := set $context.rd_feature "config" . }}
{{- include "rdeploy.context.feature" $context }}
{{- end }}


{{- range (concat $rd.infra $rd.features) }}
{{- range .repos }}

{{ $_ := set $context.rd.repos  .name .manifest }}

{{- end }}{{/*RANGE REPOS*/}}
{{- end }}{{/*RANGE INFRA/FEATURES*/}}

{{ $rd | toYaml }}

{{- end }}{{/*DEFINE*/}}

{{/*

 Filter files by name. Return true if file should be filtered

 Example:
 {{- include "rdeploy.files.filtered" (dict "input" $files_to_filter "output" $files_filtered "filter_files" $special_files) }}

 */}}

{{- define "rdeploy.files.filtered" }}

{{- $input := .input }}
{{- $output := .output }}
{{- $filter_files := .filter_files }}

{{- range $filepath, $bytes := $input }}

{{- $basename := ( include "rdeploy.files.basename" $filepath ) }}

{{- if not (hasPrefix "_" $basename) }}

{{- $is_template := true }}
{{- range $filter_file := $.filter_files }}

{{- if eq $filter_file $basename }}
{{- $is_template = false }}
{{- end }}
{{- end }}{{/*IF FILTER_FILE*/}}

{{- if $is_template }}
{{- $_ := set $output $filepath ($bytes) }}
{{- end }}{{/*IF IS_TEMPLATE*/}}

{{- end }}{{/*IF PREFIX*/}}
{{- end }}{{/*LOOP INPUT*/}}
{{- end }}{{/*DEFINE*/}}

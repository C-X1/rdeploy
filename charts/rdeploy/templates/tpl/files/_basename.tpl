{{/*

 Get Basename of filepath

 */}}

{{- define "rdeploy.files.basename" }}
{{- last ( regexSplit "/" . -1 ) }}
{{- end }}

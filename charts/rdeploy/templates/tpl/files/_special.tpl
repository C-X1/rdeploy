{{/*

 Get a list of all special files as yaml.

 Example:
 {{ $special_files := include "rdeploy.files.special" . | fromYamlArray }}

 */}}

{{- define "rdeploy.files.special" }}
- cvalues.yaml     # Calculated values of current feature - e.g. secrets
- sitemap.yaml     # Sitemap information for the current feature
- release.yaml     # Flux helm release file
- values.yaml      # Default Values for the release
- repos.yaml        # Repositories file
- example.yaml     # Yaml file containting an array of example feature configurations
{{- end }}

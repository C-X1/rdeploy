{{/*
 Reads a file, applies context and afterwards transforms it from YAML
 {{- include "rdeploy.read_yaml_with_context"
     (dict
         "context" $context
         "filepath" $filepath
         "bytes" $bytes
         "output" $output)  -}}
 */}}
{{- define "rdeploy.read_yaml_with_context" -}}

{{/*A yaml file can contain multiple documents*/}}
{{- $docs := list }}

{{- $content := $.bytes | toString }}
{{- $content := include "rdeploy.templates.fill"
(dict "text" $content "context" $.context) }}

{{- $content = regexReplaceAll "^---" $content "" }}

{{- range $part := regexSplit "\n---\n" $content -1 }}


{{ if ne "" ($part | trim) }}

{{- $part_yaml := $part | fromYaml }}
{{- $part_yaml_array := $part | fromYamlArray }}

{{- if not (hasPrefix "map[Error" ($part_yaml | toString)) }}
{{- $docs = append $docs $part_yaml }}
{{- end }}

{{- if not (hasPrefix "[error " ($part_yaml_array | toString)) }}
{{- $docs = append $docs $part_yaml_array }}
{{- end }}

{{- end }}

{{- end }}
{{- $_ := set $.output $.filepath $docs }}
{{- end -}}

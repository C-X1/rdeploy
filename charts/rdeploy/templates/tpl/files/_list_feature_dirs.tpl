{{/*
 Returns JSON list of all direct subdirectories under features/
 NOTE: To the directory being listed it must contain at least a single file in the first directory
 Example:

 {{ $myFeatures := include "rdeploy.files.list_feature_dirs" . | fromJsonArray }}

 */}}
{{- define "rdeploy.files.list_feature_dirs" -}}
{{- $feature_defaults := dict -}}
{{- range $path, $bytes := .Files.Glob "features/*/*" -}}
{{- $feature_dir := (base (dir $path)) -}}
{{- if not (hasKey $feature_defaults $feature_dir)  -}}
{{- $_ := set $feature_defaults $feature_dir (dict) -}}
{{- end -}}
{{- end -}}
{{- keys $feature_defaults | toJson -}}
{{- end -}}

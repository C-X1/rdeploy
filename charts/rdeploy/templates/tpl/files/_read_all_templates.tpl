{{/*
 Get all non-special files from a feature with the given context
 */}}
{{- define "rdeploy.read_all_templates" -}}
{{ with .context }}

{{- $special_files := include "rdeploy.files.special" . | fromYamlArray }}

{{- $files := .Files.Glob (printf "features/%s/*.yaml" .rd_feature.name) }}


{{- $files_filtered := dict }}
{{- include "rdeploy.files.filtered"
       (dict "input" $files
             "output" $files_filtered
             "filter_files" $special_files)
}}

{{- range $filepath, $bytes := $files_filtered }}

{{- include "rdeploy.read_yaml_with_context"
     (dict
         "context" $.context
         "filepath" $filepath
         "bytes" $bytes
         "output" $.output)  -}}

{{- end }}{{/*RANGE FILES*/}}


{{- end }}{{/*WITH CONTEXT*/}}
{{- end }}{{/*DEFINE*/}}

{{/*

 Replace escaped brackets in the given text.
 These are there if there is a need fir curly braces
 in the end result, for example in
 templates for external secrets.

 Example:

 {{- $test_string := include "rdeploy.templates.replace_escaped" $test_string }}

 Escape Example:
 "\\{\\{ .Values.a \\}\\}"

 */}}
{{- define "rdeploy.templates.replace_escaped" }}
{{- $text := . -}}
{{- $text := regexReplaceAll "\\\\{" $text "{" -}}
{{- $text := regexReplaceAll "\\\\}" $text "}" -}}
{{- $text }}
{{- end }}

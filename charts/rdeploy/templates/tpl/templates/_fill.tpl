{{/*

 Fill templates to a maximum level

 This enables templates to resolve templates
 containing templates for multiple levels

 Example:
 {{- include "rdeploy.templates.fill" (dict "text" $text "context" .) }}

 */}}

{{- define "rdeploy.templates.fill" }}
{{- $text := .text }}
{{- range $_, $_ := until (.max_lvl | default 20) -}}
{{- $text = tpl $text $.context }}
{{- end }}
{{- $text }}
{{- end }}

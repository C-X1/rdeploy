{{- define "rdeploy.main" }}

{{- $context := include "rdeploy.context.config_context" . | fromYaml }}

{{ $feature_list := $context.features }}
{{- if .Values.infrastructure.enabled }}
{{ $feature_list = concat  $context.infra $context.features }}
{{- end }}

{{ $output_string := "" }}

{{/* REPOS */}}
{{- range $name, $manifest := $context.repos }}
{{ $output_string = print
$output_string
($manifest | toYaml) "\n"
"---\n"
}}
{{- end }}{{/*RANGE TEMPLATES*/}}

{{/* TEMPLATES */}}
{{- range $feature := $feature_list }}

{{- range $path, $docs := $feature.templates }}
{{- range $docs }}
{{ $output_string = print
$output_string
"# " $path "\n"
(. | toYaml) "\n"
"---\n"
}}
{{- end }}{{/*RANGE DOCS*/}}
{{- end }}{{/*RANGE FEATURE.TEMPLATES*/}}


{{- if $feature.release }}
{{/* RELEASE */}}
{{- $output_string = print
$output_string
"# features/" $feature.name "/release.yaml\n"
($feature.release | toYaml) "\n"
"---\n"
}}
{{- end }}

{{- end }}{{/*RANGE FEATURE_LIST*/}}

{{- $output_string }}

{{- end }}

Feature: feature.yaml

Scenario: Template Output

Given the chart "rdeploy-foss"

Given the file "features/test/test.yaml" with content:
"""
release: {{ .rd_feature._release_name }}
repo: {{ .rd_feature | quote }}
"""


Given the file "features/test/empty.yaml" with content:
"""
"""

Given the file "values.yaml" with content:
"""
default:
  feature_namespace: "{{ .Release.Namespace }}"
  repo_namespace: "{{ .rd_feature.namespace }}"
  infra_namespace: "infra"
  reconcile_interval: 10m
  domain: "{{ .rd_feature._namespace }}.ingress.local"
  hostname: "{{ .rd_feature.release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"

features:
  - name: test
    release_name: test0
  - name: test
    release_name: test1

infrastructure:
  enabled: true
  features:
    - name: test
      release_name: infra0
    - name: test
      release_name: infra1
"""

When helm template is run

Then the result[0] should contain the following keys:
"""
release: infra0
"""
Then the result[1] should contain the following keys:
"""
release: infra1
"""
Then the result[2] should contain the following keys:
"""
release: test0
"""
Then the result[3] should contain the following keys:
"""
release: test1
"""

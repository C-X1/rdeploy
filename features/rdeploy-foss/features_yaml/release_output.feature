Feature: feature.yaml

Scenario: Release Output

Given the chart "rdeploy-foss"

Given the file "features/test/release.yaml" with content:
"""
---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: helm_test
spec:
  interval: 5m
  chart:
    spec:
      chart: helm_test_chart
      version: ">=1.0.0 <2.0.0"
      sourceRef:
        kind: HelmRepository
        name: helm_test_repo
        namespace: repo
      interval: 1m
"""

Given the file "values.yaml" with content:
"""
default:
  feature_namespace: "{{ .Release.Namespace }}"
  repo_namespace: "repo"
  infra_namespace: "infra"
  reconcile_interval: 10m
  domain: "{{ .rd_feature._namespace }}.ingress.local"
  hostname: "{{ .rd_feature.release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"

features:
  - name: test
    release_name: test0
  - name: test
    release_name: test1
    namespace: t1

infrastructure:
  enabled: true
  features:
    - name: test
      release_name: infra0
    - name: test
      release_name: infra1
      namespace: i1
"""

When helm template is run

Then the result[0] should contain the following keys:
"""
metadata:
  name: infra0
  namespace: infra
"""
Then the result[1] should contain the following keys:
"""
metadata:
  name: infra1
  namespace: i1
"""
Then the result[2] should contain the following keys:
"""
metadata:
  name: test0
  namespace: rdeploy_ns
"""
Then the result[3] should contain the following keys:
"""
metadata:
  name: test1
  namespace: t1
"""

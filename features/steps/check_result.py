"""Check helm results."""

from typing import Any

import yaml
from behave import then, use_step_matcher
from behave.runner import Context

use_step_matcher("re")


def _list_processing(  # noqa: WPS231, WPS210
    level: list, wanted: list, location: str = "root"  # noqa: ANN401  # noqa: ANN401
) -> None:
    """Process list entries.

    :param level: The variable to check / current level
    :param wanted: The state to check
    :param location: Location information in case of error
    :raises AssertionError: if an list entry is not found
    """
    used_entries = []
    for index, entry in enumerate(wanted):
        entry_missing = True
        for lvl_index, check_level in enumerate(level):
            if lvl_index in used_entries:
                continue

            try:  # noqa: WPS229
                assert_existing_structure(check_level, entry, f"{location}[{index}]")
                entry_missing = False
                used_entries.append(lvl_index)
            except AssertionError:
                pass  # noqa: WPS420

        if entry_missing:
            raise AssertionError(f"{location}[{index}] not found!")


def _dict_processing(  # noqa: WPS231
    level: dict, wanted: dict, location: str = "root"
) -> None:
    """Process list entries.

    :param level: The variable to check / current level
    :param wanted: The state to check
    :param location: Location information in case of error
    :raises AssertionError: if key is not found in dictionary
    """
    for key in wanted.keys():
        nlevel = level.get(key)
        if nlevel is not None:
            assert_existing_structure(nlevel, wanted[key], location=f"{location}.{key}")
        else:
            raise AssertionError(f"{key} not found in {location}")


def assert_existing_structure(
    level: Any, wanted: Any, location: str = "root"  # noqa: ANN401  # noqa: ANN401
) -> None:
    """Check structure for values inside.

    :param level: The variable to check / current level
    :param wanted: The state to check
    :param location: Location information in case of error
    :raises AssertionError: if no list is found but expected
    :raises AssertionError: if no dict is found but expected
    :raises AssertionError: on value missmatch
    """
    expected_type = type(wanted)
    if not isinstance(level, expected_type):
        raise AssertionError(f"'{location}' not a {expected_type}")

    if isinstance(wanted, list):
        _list_processing(level, wanted, location)
    elif isinstance(wanted, dict):
        _dict_processing(level, wanted, location)
    elif level != wanted:
        raise AssertionError(f"'{location}' missmatch")


@then(r"the result(?:\[)?(?P<doc_num>[0-9]+)?(?:\])? should contain the following keys")
def helm_run_template(context: Context, doc_num: str) -> None:
    """Check context result for the given keys.

    :param context: Behave context
    :param doc_num: contains integer string number of selected document if applicable
    :raises AssertionError: if yaml text is invalid inside a multi document
    :raises AssertionError: if yaml text is invalid inside a single document
    :raises AssertionError: if yaml text is invalid inside the feature description
    """
    result_txt = context.result
    result = None

    if doc_num:
        result_docs = result_txt.split("\n---\n")
        try:
            result_docs = [yaml.safe_load(doc) for doc in result_docs]
        except yaml.scanner.ScannerError:
            raise AssertionError("The result multidoc does not contain valid yaml")

        result_docs = [doc for doc in result_docs if doc]

        result = result_docs[int(doc_num)]

    else:
        try:
            result = yaml.safe_load(result_txt)
        except yaml.scanner.ScannerError:
            raise AssertionError("The result doc does not contain valid yaml")

    try:
        expect = yaml.safe_load(context.text)
    except yaml.scanner.ScannerError:
        raise AssertionError("Invalid yaml in feature test definition!")

    assert_existing_structure(result, expect)

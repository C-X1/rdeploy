"""Run helm."""

from behave import use_step_matcher, when
from behave.runner import Context
from rpycihelpers.execute import execute_command

use_step_matcher("re")


@when("helm template is run")
def helm_run_template(context: Context) -> None:
    """Run helm template and save result to context.

    :param context: Behave context
    """
    helm_command = "helm template deployment . --namespace rdeploy_ns"
    exec_result = execute_command(helm_command)

    error = exec_result[0] != 0

    if error:
        exec_result_debug = execute_command(f"{helm_command} --debug")
        debug = "DEBUG:\nSTDOUT:\n{}\nSTDERR:\n{}".format(
            exec_result_debug[1],
            exec_result_debug[2]
        )

    assert not error, debug

    context.result = exec_result[1]

"""Implement steps to import charts to the test folder."""

import os
import shutil

from behave import given, use_step_matcher
from behave.runner import Context
from rpycihelpers.execute import execute_command

use_step_matcher("re")


@given('the chart "(?P<chartname>.+)"(?P<fileselect> with the files)?')
def chart_files(
    context: Context, chartname: str, fileselect: str
) -> None:  # noqa: WPS213
    """Create test chart from chart.

    :param context: Behave context
    :param chartname: The name (dir) of the chart in charts directory
    :param fileselect: Contains the string "with the files"
                       if user decides to filter for files
                       with a whitelist
    """
    if fileselect:
        execute_command("helm create .")
        shutil.rmtree("templates")
        os.makedirs("templates", exist_ok=True)
        shutil.copyfile(
            os.path.join(context.chart_dir, chartname, "values.yaml"), "values.yaml"
        )

        for row in context.table.rows:
            path = row.as_dict()["path"]
            src = os.path.join(context.chart_dir, chartname, path)
            if os.path.isdir(src):
                os.makedirs(path, exist_ok=True)
                shutil.copytree(src, path, dirs_exist_ok=True)
            else:
                dirname = os.path.dirname(path)
                if dirname:
                    os.makedirs(dirname, exist_ok=True)
                shutil.copyfile(src, path)
    else:
        chart_dir = os.path.join(context.chart_dir, chartname)
        helm_prepare_command = (
            f"sh -c 'cd {chart_dir} rm charts/* Chart.lock;"
            " helm dependency build --skip-refresh'"
        )
        helm_prepare_result = execute_command(helm_prepare_command)

        assert not helm_prepare_result[0], (
            f"Preparing helm chart failed: " f"{helm_prepare_result}"
        )

        shutil.copytree(
            os.path.join(context.chart_dir, chartname), os.getcwd(), dirs_exist_ok=True
        )

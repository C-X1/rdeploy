"""Create files."""

import os

import yaml
from behave import given, use_step_matcher
from behave.runner import Context

use_step_matcher("re")


@given('the (?P<is_yaml>yaml )?file "(?P<filepath>.+)"(?P<filecontent> with content)?')
def create_file(context: Context, is_yaml: str, filepath: str, filecontent: str) -> None:
    """Create a file with text content.

    :param context: Behave context
    :param is_yaml: contains the word yaml if given
    :param filepath: The path of the created file
    :param filecontent: Contains the string "with content"
                       if user decides to have content.
    :raises AssertionError: if yaml text is invalid
    """
    file_content = ""
    if filecontent:
        file_content = context.text
    if is_yaml:
        try:
            yaml.safe_load(file_content)
        except yaml.scanner.ScannerError:
            raise AssertionError("Invalid yaml in feature test definition!")

    directory = os.path.dirname(filepath)
    if directory:
        os.makedirs(directory, exist_ok=True)
    with open(filepath, "w") as text_file:
        text_file.write(file_content)

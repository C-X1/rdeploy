"""Implement Behave Environment."""

import os
import sys

from behave.runner import Context
from behave.model import Scenario
from rpycihelpers.foldered_test import FolderedBehaveTest

def before_all(context: Context) -> None :
    """Prepare all test.

    :param context: The behave context
    """
    context.project_dir = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..")
    )
    context.chart_dir = os.path.join(context.project_dir, "charts")
    context.foldered_test = FolderedBehaveTest()


def before_scenario(context: Context, scenario: Scenario) -> None:
    """Prepare the scenario.

    :param context: The behave context
    :param scenario: The scenario settings
    """
    context.foldered_test.setup(scenario)


def after_scenario(context: Context, scenario: Scenario) -> None:
    """Prepare the scenario.

    :param context: The behave context
    :param scenario: The scenario settings
    """
    context.foldered_test.teardown()
    sys.stdout.flush()
    if "debug" in scenario.tags:
        sys.stdout.write("### DEBUG:\n")
        if "result" in context:
            sys.stdout.write(context.result)
        else:
            sys.stdout.write("No result entry in behave context.")
        sys.stdout.flush()

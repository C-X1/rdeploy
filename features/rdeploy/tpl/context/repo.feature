Feature: Repos in Context

Scenario: Reading repositories

Given the chart "rdeploy" with the files:
  | path          |
  | templates/tpl |

Given the file "features/test/repos.yaml" with content:
"""
---
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: example-repo1
  namespace: flux-system
spec:
  interval: 1m
  ref:
    branch: main
  url: https://github.com/example/example-repo1.git
  secretRef:
    name: example-repo-secret
---
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: example-repo2
  namespace: flux-system
spec:
  interval: 1m
  ref:
    branch: main
  url: https://github.com/example/example-repo2.git
  secretRef:
    name: example-repo-secret
"""

Given the file "values.yaml" with content:
"""
default:
  repo_namespace: "{{ .rd_feature._namespace }}"
  infra_namespace: "infra"
  reconcile_interval: 10m
  domain: "{{ .rd_feature._namespace }}.ingress.local"
  hostname: "{{ .rd_feature.release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"

features:
  - name: test
  - name: test
    release_name: test2
    namespace: test2

infrastructure:
  features:
    - name: test
      release_name: itest1
    - name: test
      namespace: itest2_ns
      release_name: itest2
"""

Given the file "templates/test.yaml" with content:
"""
---
{{- include "rdeploy.context.config_context" . }}
"""

When helm template is run

Then the result should contain the following keys:
"""
infra:
  - _repo_namespace: infra
    repos:
      - name: deployment-example-repo1-1bda314
        manifest:
          kind: GitRepository
      - name: deployment-example-repo2-ca0ceaf
        manifest:
          kind: GitRepository
  - _repo_namespace: itest2_ns
    repos:
      - name: deployment-example-repo1-075c066
        manifest:
          kind: GitRepository
      - name: deployment-example-repo2-0e1e429
        manifest:
          kind: GitRepository

features:
  - _repo_namespace: rdeploy_ns
    repos:
      - name: deployment-example-repo1-dbf1d14
        manifest:
          kind: GitRepository
      - name: deployment-example-repo2-14a323b
        manifest:
          kind: GitRepository
  - _repo_namespace: test2
    repos:
      - name: deployment-example-repo1-52bc1a2
        manifest:
          kind: GitRepository
      - name: deployment-example-repo2-6fca3d6
        manifest:
          kind: GitRepository
"""

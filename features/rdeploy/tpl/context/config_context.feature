Feature: Overall Context

@debug
Scenario: Output context as yaml

Given the chart "rdeploy" with the files:
  | path          |
  | templates/tpl |

Given the file "features/test/sitemap.yaml" with content:
"""
- url: "{{ .Values.default.uri }}"
  label: "Test Instance {{ .Release.Name }}"
"""

Given the file "features/test/cvalues.yaml" with content:
"""
a: {{ .rd_feature.name }}
"""

Given the file "features/test/test.yaml" with content:
"""
---
my_name: "{{ .rd_feature._release_name }}"
---
this_should_be_nope: {{ (default .rd "nope") | toString }}
feature_name_12345: "{{ .rd_feature.cvalues.a  }}_12345"
default_b: "{{ .rd_feature.defaults.b }}"
"""

Given the file "values.yaml" with content:
"""
feature_defaults:
  test:
    a: 1
    b: "{{ .Release.Name }}_b"
default:
  feature_namespace: "{{ .Release.Namespace }}"
  repo_namespace: "{{ .rd_feature._namespace }}"
  infra_namespace: "infra"
  reconcile_interval: 10m
  domain: "{{ .rd_feature._namespace }}.ingress.local"
  hostname: "{{ .rd_feature.release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"

features:
  - name: test
  - name: test
    release_name: test2
    namespace: test2

infrastructure:
  features:
    - name: test
      release_name: itest1
    - name: test
      namespace: itest2_ns
      release_name: itest2
"""

Given the file "templates/test.yaml" with content:
"""
---
{{- include "rdeploy.context.config_context" . }}
"""

When helm template is run

Then the result should contain the following keys:
"""
features:
  - name: test
    _release_name: test
    _namespace: rdeploy_ns
    templates:
      features/test/test.yaml:
        - my_name: test
        - this_should_be_nope: nope
          feature_name_12345: test_12345
          default_b: deployment_b

  - name: test
    namespace: test2
    release_name: test2
    _release_name: test2
    _namespace: test2
    templates:
      features/test/test.yaml:
        - my_name: test2
        - this_should_be_nope: nope
    Values: {}
    sitemap:
      - label: Test Instance deployment
        url: https://test2.test2.ingress.local

infra:
  - name: test
    release_name: itest1
    _release_name: itest1
    _namespace: infra
  - name: test
    release_name: itest2
    _release_name: itest2
    _namespace: itest2_ns
"""

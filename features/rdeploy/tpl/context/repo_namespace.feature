Feature: Repo Namespace

Scenario Outline: Get repo namespace for feature

Given the chart "rdeploy" with the files:
  | path          |
  | templates/tpl |

Given the file "features/test/test.yaml" with content:
"""
repo_namespace: {{ .rd_feature._repo_namespace }}
"""

Given the file "values.yaml" with content:
"""
default:
  feature_namespace: "{{ .Release.Namespace }}"
  repo_namespace: "<repo_namespace>"
  infra_namespace: "<infra_ns>"
  reconcile_interval: 10m
  domain: "{{ .rd_feature._namespace }}.ingress.local"
  hostname: "{{ .rd_feature.release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"

features:
  - name: test
  - name: test
    repo_namespace: override

infrastructure:
  features:
    - name: test
    - name: test
      repo_namespace: override
"""

Given the file "templates/test.yaml" with content:
"""
---
{{- include "rdeploy.context.config_context" . }}
"""

When helm template is run

Then the result should contain the following keys:
"""
features:
  - _repo_namespace: <repo_feature0>
  - _repo_namespace: override
infra:
  - _repo_namespace: <repo_infra0>
  - _repo_namespace: override
"""

  Examples:
    | repo_namespace | repo_feature0 | repo_infra0 | infra_ns |
    | repo           | repo          | repo        | infra    |
    |                | rdeploy_ns    | infra       | infra    |

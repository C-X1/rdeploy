Feature: feature.yaml

@debug
Scenario: Output

Given the chart "rdeploy"

Given the file "Chart.yaml" with content:
"""
apiVersion: v2
name: rdeploy-test
type: application
version: 0.1.0
"""

Given the file "features/test/empty_template_file.yaml" with content:
"""
---

"""

Given the file "features/test/empty_template_file2.yaml" with content:
"""

"""

Given the file "features/test/something.yaml" with content:
"""
something: 1
"""

Given the file "features/test/somethingelse.yaml" with content:
"""
somethingelse: 0
"""

Given the file "templates/features.yaml" with content:
"""
---
{{- include "rdeploy.main" . }}
"""

Given the file "values.yaml" with content:
"""
---
default:
  feature_namespace: "{{ .Release.Namespace }}"
  repo_namespace: repo
  infra_namespace: "infra"
  reconcile_interval: 10m
  domain: "{{  .rd_feature._namespace }}.ingress.local"
  hostname: "{{  .rd_feature._release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"  # Keycloak token in URL

features:
  - name: test

infrastructure:
  enabled: true
  features: []
"""

When helm template is run

Then the result[0] should contain the following keys:
"""
something: 1
"""

Then the result[1] should contain the following keys:
"""
somethingelse: 0
"""

Feature: List feature directories

Scenario: Feature directories

Given the chart "rdeploy" with the files:
  | path                              |
  | templates/tpl/files/_list_feature_dirs.tpl |

Given the file "features/test1/test.yaml"
Given the file "features/test2/test.yaml"

Given the file "templates/test.yaml" with content:
"""
---
features: {{ include "rdeploy.files.list_feature_dirs" . }}
"""

When helm template is run
Then the result should contain the following keys:
"""
features:
  - test1
  - test2
"""

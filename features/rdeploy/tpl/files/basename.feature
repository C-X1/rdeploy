Feature: Extract basename from filepath

Scenario Outline: Basename

Given the chart "rdeploy" with the files:
  | path                              |
  | templates/tpl/files/_basename.tpl  |

Given the file "templates/test.yaml" with content:
"""
---
basename: {{ include "rdeploy.files.basename" "<path>" }}
"""

When helm template is run
Then the result should contain the following keys:
"""
basename: "<basename>"
"""

Examples:
  | path              | basename   |
  | a/b/c/d.yaml      | d.yaml     |
  | /hello/world.yaml | world.yaml |
  | ../../up.txt      | up.txt     |

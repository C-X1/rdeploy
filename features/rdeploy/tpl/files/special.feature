Feature: List special files

Scenario: Special Files

Given the chart "rdeploy" with the files:
  | path                              |
  | templates/tpl/files/_special.tpl |

Given the file "templates/test.yaml" with content:
"""
---
special_files: {{ include "rdeploy.files.special" . }}
"""

When helm template is run
Then the result should contain the following keys:
"""
special_files:
  - repos.yaml
  - release.yaml
  - values.yaml
  - sitemap.yaml
  - example.yaml
  - cvalues.yaml
"""

Feature: Read yaml files

Scenario: Read yaml files

Given the chart "rdeploy" with the files:
  | path                                            |
  | templates/tpl/files/_read_yaml_with_context.tpl |
  | templates/tpl/templates/_fill.tpl               |

Given the file "features/test1/test.yaml" with content:
"""
- 1
- 2
- a:
   b:
    c:
     d: {{ .test_string }}
---
a:
  b:
    c:
      d: {{ .test_string }}
"""

Given the file "features/test1/empty.yaml" with content:
"""
"""

Given the file "templates/test.yaml" with content:
"""
---
{{- $context := merge (dict "test_string" "abcd") . }}
{{- $output := dict }}
{{- range $filepath, $bytes := .Files.Glob "features/**" }}
{{- include "rdeploy.read_yaml_with_context"
     (dict
         "context" $context
         "filepath" $filepath
         "bytes" $bytes
         "output" $output)  -}}
{{- end }}

files:
{{ $output | toYaml | indent 2 }}
"""

When helm template is run
Then the result should contain the following keys:
"""
files:
  features/test1/test.yaml:
  - - 1
    - 2
    - a:
       b:
        c:
         d: abcd
  - a:
      b:
        c:
          d: abcd
"""

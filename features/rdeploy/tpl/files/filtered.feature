Feature: Filter directories

Scenario: Filter feature directories

Given the chart "rdeploy" with the files:
  | path                              |
  | templates/tpl/files/_filtered.tpl |
  | templates/tpl/files/_basename.tpl                          |

Given the file "features/test1/f1.yaml"
Given the file "features/test1/test1.yaml"
Given the file "features/test1/f2.yaml"
Given the file "features/test1/test2.yaml"
Given the file "features/test1/_not_there.yaml"

Given the file "templates/test.yaml" with content:
"""
---
{{- $output := dict }}
{{- include "rdeploy.files.filtered"
       (dict "input" (.Files.Glob "features/**")
             "output" $output
             "filter_files" (list "f1.yaml" "f2.yaml"))
}}
filtered_files:
{{ $output | toYaml | indent 2 }}
"""

When helm template is run
Then the result should contain the following keys:
"""
filtered_files:
  "features/test1/test1.yaml": ""
  "features/test1/test2.yaml": ""
"""

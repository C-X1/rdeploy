Feature: Read all templates from a feature

Scenario: Read all templates from a feature

Given the chart "rdeploy" with the files:
  | path                              |
  | templates/tpl/files               |
  | templates/tpl/templates/_fill.tpl |

Given the file "features/test1/releasename.yaml" with content:
"""
release-name: {{ .Release.Name }}
"""

Given the file "features/test1/namespace.yaml" with content:
"""
namespace: {{ .Release.Name }}
---
data:
  a: 1
  b: 2
"""

Given the file "features/test1/empty.yaml" with content:
"""
"""

Given the file "templates/test.yaml" with content:
"""
---
{{ $output := dict }}
{{ $context := merge (dict "rd_feature" (dict "name" "test1")) . }}

{{ include "rdeploy.read_all_templates"
    (dict "context" $context
          "output" $output
    )
}}

{{ $output | toYaml }}
"""

When helm template is run

Then the result should contain the following keys:
"""
"features/test1/namespace.yaml":
  - namespace: deployment
  - data:
      a: 1
      b: 2
"features/test1/releasename.yaml":
  - release-name: deployment
"""

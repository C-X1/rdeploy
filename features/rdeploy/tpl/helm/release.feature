Feature: Helm Release

Scenario: Helm release from feature


Given the chart "rdeploy" with the files:
  | path          |
  | templates/tpl |

Given the file "features/helm_test/release.yaml" with content:
"""
---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: helm_test
spec:
  interval: 5m
  chart:
    spec:
      chart: helm_test_chart
      version: ">=1.0.0 <2.0.0"
      sourceRef:
        kind: HelmRepository
        name: helm_test_repo
        namespace: repo
      interval: 1m
"""

Given the file "features/helm_test/values.yaml" with content:
"""
release_values:
  namespace: {{ .Release.Namespace }}
  name: {{ .Release.Name }}
  hostname: {{ .Values.default.hostname }}
  uri: {{ .Values.default.uri }}
"""

Given the yaml file "values.yaml" with content:
"""
default:
  feature_namespace: "{{ .Release.Namespace }}"
  repo_namespace: "{{ .rd_feature.namespace }}"
  infra_namespace: "infra"
  reconcile_interval: 10m
  domain: "{{  .rd_feature._namespace }}.ingress.local"
  hostname: "{{  .rd_feature._release_name }}.{{ .Values.default.domain }}"
  uri: "https://{{ .Values.default.hostname }}"
  ingress_annotations:
    nginx.ingress.kubernetes.io/proxy-buffer-size: "32k"  # Keycloak token in URL

features:
  - name: helm_test

infrastructure:
  enabled: true
  features: []
"""

Given the file "templates/test.yaml" with content:
"""
{{- $context := merge dict . }}
{{- $_ := set $context "rd_feature"
    (dict
      "name" "helm_test"
      "_release_name" "helm_test_rn"
      "_namespace" "helm_test_ns"
    )
}}

{{ include "rdeploy.helm.release" $context }}
"""

When helm template is run

Then the result should contain the following keys:
"""
kind: HelmRelease
metadata:
  name: helm_test_rn
  namespace: helm_test_ns
spec:
  interval: 5m
  values:
    release_values:
      hostname: helm_test_rn.helm_test_ns.ingress.local
      name: deployment
      namespace: rdeploy_ns
      uri: https://helm_test_rn.helm_test_ns.ingress.local
"""

Feature: Better dig for extracting nested structures

Scenario: Using bdig to extract


Given the chart "rdeploy" with the files:
  | path                        |
  | templates/tpl/dig/_bdig.tpl |
Given the yaml file "values.yaml" with content:
"""
a:
  b:
    c:
      - d:
          'test.yaml':
            - test0
            - '1000': 'ok12345'
      - 1000
"""

Given the file "templates/test.yaml" with content:
"""
abc0testyaml11000: '{{- include "rdeploy.dig.bdig" (list "a.b.c.0.d.test\\.yaml.1.1000" .Values) -}}'
c1: {{ (include "rdeploy.dig.bdig" (list "a.b.c.1" .Values)) -}}
"""

When helm template is run
Then the result should contain the following keys:
"""
abc0testyaml11000: 'ok12345'
c1: 1000
"""

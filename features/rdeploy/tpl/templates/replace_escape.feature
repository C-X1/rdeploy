Feature: Replace escaped brackets

Scenario: Replace escaped brackets

Given the chart "rdeploy" with the files:
  | path                                        |
  | templates/tpl/templates/_replace_escape.tpl |
  | templates/tpl/templates/_fill.tpl           |

Given the file "values.yaml" with content:
"""
a: b
b: "\\{\\{ .Values.a \\}\\}"
"""

Given the file "templates/test.yaml" with content:
"""
{{- $test_string := .Values.b }}
{{- $test_string := include "rdeploy.templates.fill" (dict "text" $test_string "context" .) }}
{{- $test_string := include "rdeploy.templates.replace_escaped" $test_string }}
result: {{ $test_string | quote }}
"""

When helm template is run
Then the result should contain the following keys:
"""
result: "{{ .Values.a }}"
"""

Feature: Fill templates

Scenario: Fill templates

Given the chart "rdeploy" with the files:
  | path                                        |
  | templates/tpl/templates/_fill.tpl           |

Given the file "values.yaml" with content:
"""
a: abcdef
b: '{{ .Values.a }}'
c: '{{ .Values.b }}'
d: '{{ .Values.c }}'
e: '{{ .Values.d }}'
"""

Given the file "templates/test.yaml" with content:
"""
f: {{ include "rdeploy.templates.fill" (dict "text" "{{ .Values.e }}" "context" .) }}
"""

When helm template is run
Then the result should contain the following keys:
"""
f: "abcdef"
"""
